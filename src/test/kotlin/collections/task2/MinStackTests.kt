package collections.task2

import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import java.util.*
import kotlin.random.Random

class MinStackTests : StringSpec({
    "min with empty set test" {
        val stack: MinStack<Int> = MinStack()

        shouldThrow<EmptyStackException> {
            stack.min()
        }
    }
    "push null test" {
        val stack: MinStack<Int> = MinStack()

        shouldThrow<NullPointerException> {
            stack.push(null)
        }
    }
    "min test" {
        val stack: MinStack<Int> = MinStack()

        val elementsCount = 10000
        val randomValues = MutableList(elementsCount) { Random.nextInt() }

        var min: Int = Int.MIN_VALUE
        randomValues.forEach {
            stack.push(it)
            if (it < min) min = it
            stack.min() shouldBe min
        }

        repeat(100) {
            stack.peek()
            stack.min() shouldBe min
        }

        repeat(elementsCount) {
            val element = stack.pop()
            randomValues.remove(element)
            stack.min() shouldBe randomValues.min()
        }

        stack.pop()
        shouldThrow<NullPointerException> {
            stack.push(null)
        }
    }
})