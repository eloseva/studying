package collections.task1

import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import java.util.*
import kotlin.random.Random

class StackTests : StringSpec ({
    "push, pop and peek test" {
        val stack: Stack<Int> = Stack()

        val randomValues = List(10000) { Random.nextInt() }

        randomValues.forEach {
            stack.push(it)
            stack.peek() shouldBe it
        }

        randomValues.reversed().forEach {
            stack.peek() shouldBe it
            stack.pop() shouldBe it
        }

        repeat(100) {
            shouldThrow<EmptyStackException> {
                stack.pop()
            }
        }
    }
    "size and isEmpty test" {
        val stack: Stack<Int> = Stack()

        stack.size() shouldBe 0
        stack.isEmpty shouldBe true

        val elementsCount = 10000
        val randomValues = List(elementsCount) { Random.nextInt() }

        randomValues.forEachIndexed { index, element ->
            stack.push(element)
            stack.size() shouldBe index + 1
            stack.isEmpty shouldBe false
        }

        repeat(100) {
            stack.peek()
            stack.size() shouldBe elementsCount
            stack.isEmpty shouldBe false
        }

        randomValues.reversed().forEachIndexed { index, _ ->
            stack.size() shouldBe elementsCount - index
            stack.isEmpty shouldBe false
            stack.pop()
        }

        stack.size() shouldBe 0
        stack.isEmpty shouldBe true
    }
})