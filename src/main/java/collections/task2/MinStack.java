package collections.task2;

import collections.task1.Stack;
import shared.NotImplementedException;

/**
 * <p>{@link Stack} extension that provides additional method returning min element contained in stack.</p>
 * <p>Method {@link #min()} should have constant (O(1)) time complexity.</p>
 * <p>{@link MinStack} should not accept {@code null} elements.</p>
 *
 * @param <E> type of elements to store
 * @see <a href="https://en.wikipedia.org/wiki/Time_complexity">time complexity</a>
 */
public class MinStack<E extends Comparable<E>> extends Stack<E> {

    /**
     * Returns the min element contained in stack.
     * @throws java.util.EmptyStackException in case stack is empty
     * @return the min element contained in stack
     */
    public E min() {
        throw new NotImplementedException();
    }

    /**
     * Adds new element to the end of stack.
     * @param element element to add
     * @throws NullPointerException in case element is {@code null}
     */
    @Override
    public void push(E element) {
        super.push(element);
    }
}